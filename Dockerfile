FROM mcr.microsoft.com/windows/servercore/insider:10.0.20348.1-amd64

# Setup shell
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; "]

# Download vs_BuildTools.exe
RUN Invoke-WebRequest -Uri "https://download.visualstudio.microsoft.com/download/pr/11168600/e64d79b40219aea618ce2fe10ebd5f0d/vs_BuildTools.exe" -OutFile vs_BuildTools.exe

# Install MSBuild, Component.CoreBuildTools, and Component.Roslyn.Compiler

RUN Start-Process -FilePath ./vs_BuildTools.exe -NoNewWindow -Wait -ArgumentList '--quiet', '--wait', '--add Microsoft.VisualStudio.Workload.MSBuildTools', '--includeRecommended' ;

# Add MSBuild to $PATH
RUN $env:PATH = 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\BuildTools\\MSBuild\\15.0\\Bin\\;' + $env:PATH; \
    [Environment]::SetEnvironmentVariable('PATH', $env:PATH, [EnvironmentVariableTarget]::Machine);

RUN rm ./vs_BuildTools.exe

CMD ["powershell"]
